﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Egov.Data.Business;

namespace Egov.Data.Models
{
    public partial class da_Place
    {
        private string serverPath = System.Configuration.ConfigurationManager.AppSettings["ImagePath"];

        public List<string> PlaceImages
        {
            get
            {
                bl_PlaceImage blPlaceImage = new bl_PlaceImage();
                var imgs = blPlaceImage.GetByPlaceId(Id);
                string imgLink = "";
                
                List<string> placeImages = new List<string>();
                foreach(var item in imgs)
                {
                    if (item.ImageName.IndexOf("http") <= 0)
                        imgLink = serverPath + item.ImageName;
                    else
                        imgLink = item.ImageName;
                    placeImages.Add(imgLink);
                }
                return placeImages;
            }
        }

        public string Image
        {
            get
            {
                if (PlaceImages.Count > 0)
                    return PlaceImages.First();
                else
                    return null;
            }
        }
    }
}
