//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Egov.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class da_Location
    {
        public da_Location()
        {
            this.da_Location1 = new HashSet<da_Location>();
            this.da_Place = new HashSet<da_Place>();
        }
    
        public System.Guid Id { get; set; }
        public string LocationName { get; set; }
        public Nullable<System.Guid> ParentId { get; set; }
    
        public virtual ICollection<da_Location> da_Location1 { get; set; }
        public virtual da_Location da_Location2 { get; set; }
        public virtual ICollection<da_Place> da_Place { get; set; }
    }
}
