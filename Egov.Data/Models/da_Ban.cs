//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Egov.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class da_Ban
    {
        public int id { get; set; }
        public int userid { get; set; }
        public string note { get; set; }
        public System.DateTime timeban { get; set; }
        public System.DateTime createdate { get; set; }
        public System.DateTime modify { get; set; }
        public Nullable<bool> status { get; set; }
    
        public virtual da_UserProfile da_UserProfile { get; set; }
    }
}
