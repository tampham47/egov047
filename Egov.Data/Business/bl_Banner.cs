﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Banner
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Banner(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public List<st_Banner_GetAll_Result> GetAll()
        {
            return db.st_Banner_GetAll().ToList();
        }

        public List<st_Banner_GetAllByStatus_Result> GetAllByStatus(bool Status)
        {
            var result = db.st_Banner_GetAllByStatus(Status).ToList();
            return result;
        }
        
        public int Insert(Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            var result = db.st_Banner_Insert(userid, timeban, note, status).Single();

            return (int)result;
        }

        public int Update(Nullable<int> id, Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            var result = db.st_Banner_Update(
                id, userid, timeban, note,
                status).Single();
            return (int)result;
        }
    }
}
