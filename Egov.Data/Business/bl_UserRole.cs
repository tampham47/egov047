﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_UserRole
    {
        Egov047Entities db = new Egov047Entities();
        
        public bl_UserRole(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public int Insert(int groupId, string roleName, string type)
        {
            var result = db.st_Roles_Insert(groupId, roleName, type).Single();
            return (int)result;
        }

        public List<da_Roles> GetAll()
        {
            return db.st_Roles_GetAll().ToList();
        }

        public List<da_Roles> GetByGroupId(int groupId)
        {
            return db.st_Roles_GetByGroupId(groupId).ToList();
        }

        public List<da_Roles> GetByName(string roleName)
        {
            return db.st_Roles_GetByName(roleName).ToList();
        }

        public List<da_Roles> GetByIdAndType(int groupId, string type)
        {
            return db.st_Roles_GetByIdAndType(groupId, type).ToList();
        }

        public List<da_Roles> GetByType(string type)
        {
            return db.st_Roles_GetByType(type).ToList();
        }

        public int Delete(int groupId, string roleName)
        {
            return (int)db.st_Roles_Delete(groupId, roleName).Single();
        }

        public int DeleteByGroupId(int groupId)
        {
            return (int)db.st_Roles_DeleteByGroupId(groupId).Single();
        }

        public int DeleteByName(string roleName)
        {
            return (int)db.st_Roles_DeleteByName(roleName).Single();
        }
    }
}
