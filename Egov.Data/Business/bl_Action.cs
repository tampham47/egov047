﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Action
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Action(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public int Delete(Guid id)
        {
            return (int)db.st_Action_Delete(id).Single();
        }
        public List<da_Action> GetAll()
        {
            return db.st_Action_GetAll().ToList();
        }
        public da_Action GetById(Guid id)
        {
            var result = db.st_Action_GetById(id).ToList();
            if (result.Count > 0)
                return result.First();
            else
                return null;
        }
        public List<da_Action> GetByPlaceId(Guid placeId)
        {
            var result = db.st_Action_GetByPlaceId(placeId).ToList();
            return result;
        }
        public List<da_Action> GetByUserId(int userId)
        {
            var result = db.st_Action_GetByUserId(userId).ToList();
            return result;
        }
        public int Insert(Nullable<int> userId, Nullable<System.Guid> placeId, string actionType)
        {
            var result = db.st_Action_Insert(userId, placeId, 
                actionType, DateTime.Now).Single();

            return (int)result;
        }
        public int Update(Nullable<System.Guid> id, Nullable<int> userId, Nullable<System.Guid> placeId, string actionType)
        {
            var result = db.st_Action_Update(id, userId, 
                placeId, actionType, DateTime.Now).Single();

            return (int)result;
        }
        public List<da_Action> GetByType(string type)
        {
            var result = db.st_Action_GetByActionType(type).ToList();
            return result;
        }
    }
}
