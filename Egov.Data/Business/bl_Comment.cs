﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Comment
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Comment(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public List<da_Comment> GetAll()
        {
            return db.st_Comment_GetAll().ToList();
        }

        public List<st_Comment_GetAllByStatus_Result> GetAllByStatus(bool Status)
        {
            var result = db.st_Comment_GetAllByStatus(Status).ToList();
            return result;
        }

        public List<st_CommentBy_Place_Status_Result> GetByPlaceStatus(Nullable<System.Guid> placeId, Nullable<bool> status)
        {
            var result = db.st_CommentBy_Place_Status(placeId, status).ToList();
            return result;
        }

        public List<da_Comment> GetByUserId(int userId)
        {
            return db.st_Comment_GetByUserId(userId).ToList();
        }

        public List<da_Comment> GetByPlaceId(Guid placeId)
        {
            return db.st_Comment_GetPlaceId(placeId).ToList();
        }

        public int Insert(Nullable<int> userId, string content, Nullable<System.Guid> placeId, Nullable<bool> status)
        {
            var result = db.st_Comment_Insert(
                userId, content, placeId, DateTime.Now, status).Single();
            return (int)result;
        }

        public int Update(Nullable<System.Guid> id, Nullable<int> userId, Nullable<System.Guid> placeId, string content, Nullable<bool> status)
        {
            var result = db.st_Comment_Update(
                id, userId, placeId, content, status).Single();
            return (int)result;
        }

        public int Delete(Guid id)
        {
            var result = db.st_Comment_Delete(id).Single();
            return (int)result;
        }
    }
}
