﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_PlaceImage
    {
        Egov047Entities db = new Egov047Entities();
        public bl_PlaceImage(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public int Delete(Guid id)
        {
            return (int)db.st_PlaceImage_Delete(id).Single();
        }
        public List<da_PlaceImage> GetAll()
        {
            return db.st_PlaceImage_GetAll().ToList();
        }
        public da_PlaceImage GetById(Guid id)
        {
            var result = db.st_PlaceImage_GetById(id).ToList();

            if (result.Count > 0)
                return result.First();
            else
                return null;
        }
        public List<da_PlaceImage> GetByPlaceId(Guid placeId)
        {
            var result = db.st_PlaceImage_GetByPlaceId(placeId).ToList();
            return result;
        }
        public int Insert(Nullable<System.Guid> placeId, string imageName, string imagePath)
        {
            var result = db.st_PlaceImage_Insert(
                placeId, imageName, imagePath, DateTime.Now).Single();
            return (int)result;
        }
        public int Update(Nullable<System.Guid> id, Nullable<System.Guid> placeId, string imageName, string imagePath)
        {
            var result = db.st_PlaceImage_Update(id,
                placeId, imageName, imagePath, DateTime.Now).Single();
            return (int)result;
        }
    }
}
