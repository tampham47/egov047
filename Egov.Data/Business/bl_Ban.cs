﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Ban
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Ban(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public List<st_Ban_GetByAll_Result> GetAll()
        {
            return db.st_Ban_GetByAll().ToList();
        }

        public List<st_Ban_GetByStatus_Result> GetAllByStatus(bool Status)
        {
            var result = db.st_Ban_GetByStatus(Status).ToList();
            return result;
        }
        
        public int Insert(Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            var result = db.st_Ban_Insert(userid, timeban, note, status).Single();

            return (int)result;
        }

        public int Update(Nullable<int> id, Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            var result = db.st_Ban_Update(
                id, userid, timeban, note,
                status).Single();
            return (int)result;
        }

        public int TimeBan(Nullable<int> id, Nullable<System.DateTime> timeban)
        {
            var result = db.st_Ban_Update_Timeban(
                id, timeban).Single();
            return (int)result;
        }

        public int Status(Nullable<int> id, Nullable<bool> status)
        {
            var result = db.st_Ban_Update_Status(
                id, status).Single();
            return (int)result;
        }
    }
}
