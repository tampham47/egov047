﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_UserGroup
    {
        Egov047Entities db = new Egov047Entities();
        
        public bl_UserGroup(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public int Insert(string groupName, string description)
        {
            var result = db.st_UserGroup_Insert(groupName, description).Single();
            return (int)result;
        }

        public List<da_UserGroup> GetAll()
        {
            return db.st_UserGroup_GetAll().ToList();
        }

        public List<da_UserGroup> GetByID(int groupId)
        {
            return db.st_UserGroup_GetById(groupId).ToList();
        }

        public List<da_UserGroup> GetByName(string groupName)
        {
            return db.st_UserGroup_GetByName(groupName).ToList();
        }

        public int Update(int groupId, string groupName, string description)
        {
            return (int)db.st_UserGroup_Update(groupId, groupName, description).Single();
        }

        public int Delete(int groupId)
        {
            return (int)db.st_UserGroup_Delete(groupId).Single();
        }
    }
}
