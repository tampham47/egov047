﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Location
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Location(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public int Delete(Guid id)
        {
            return (int)db.st_Location_Delete(id).Single();
        }
        public List<da_Location> GetAll()
        {
            return db.st_Location_GetAll().ToList();
        }
        public List<da_Location> GetByParentId(Guid parentId)
        {
            var result = db.st_Location_GetByParentId(parentId).ToList();
            return result;
        }
        public int Insert(string locationName, Nullable<System.Guid> parentId)
        {
            var result = db.st_Location_Insert(locationName, parentId).Single();
            return (int)result;
        }
        public int Update(Nullable<System.Guid> id, string locationName, Nullable<System.Guid> parentId)
        {
            var result = db.st_Location_Update(
                id, locationName, parentId).Single();
            return (int)result;
        }
    }
}
