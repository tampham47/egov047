﻿using Egov.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egov.Data.Business
{
    public class bl_Place
    {
        Egov047Entities db = new Egov047Entities();
        public bl_Place(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public List<st_Place_GetAllByStatus_Result> GetAllByStatus(bool Status)
        {
            var result = db.st_Place_GetAllByStatus(Status).ToList();
            return result;
        }

        public List<st_PlaceBy_Location_Status_Result> GetByPlaceStatus(Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            var result = db.st_PlaceBy_Location_Status(locationId, status).ToList();
            return result;
        }

        public int Delete(Guid id)
        {
            return (int)db.st_Place_Delete(id).Single();
        }
        public List<da_Place> GetAll()
        {
            return db.st_Place_GetAll().ToList();
        }
        public List<da_Place> GetByLocationId(Guid locationId)
        {
            var result = db.st_Place_GetByLocationId(locationId).ToList();
            return result;
        }
        public List<da_Place> GetByUserId(int userId)
        {
            var result = db.st_Place_GetByUserId(userId).ToList();
            return result;
        }
        public da_Place GetById(Guid id)
        {
            var result = (
                from get in db.da_Place
                where get.Id == id
                select get
            ).ToList();

            if (result.Count > 0)
                return result.First();
            else
                return null;
        }
        public int Insert(Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            var result = db.st_Place_Insert(
                userId,title, summary, description, 
                latitude, longitude, priority,
                address, locationId, DateTime.Now, status).Single();

            return (int)result;
        }

        public Guid Insert2(Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            var id = Guid.NewGuid();
            var result = (int)db.st_Place_Insert2(
                id, userId, title, summary, description,
                latitude, longitude, priority,
                address, locationId, DateTime.Now, status).Single();

            if (result > 0)
                return id;
            else
                return Guid.Empty;
        }

        public int Update(Nullable<System.Guid> id, Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            var result = db.st_Place_Update(
                id, userId, title, summary,
                description, latitude, longitude,
                priority, address, locationId, status).Single();
            return (int)result;
        }
    }
}
