﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Egov.Data.Models;

namespace Egov.Data.Business
{
    public class bl_UserProfile
    {
        Egov047Entities db = new Egov047Entities();
        public bl_UserProfile(Egov047Entities connection = null, bool isLazy = false)
        {
            if (connection != null) db = connection;
            db.Configuration.LazyLoadingEnabled = isLazy;
        }

        public da_UserProfile GetById(int id)
        {
            var data = db.st_UserProfile_GetById(id).ToList();
            if (data.Count > 0)
                return data.First();
            else
                return null;
        }
        public da_UserProfile GetByUserName(string userName)
        {
            var data = db.st_UserProfile_GetByUserName(userName).ToList();
            if (data.Count > 0)
                return data.First();
            else
                return null;
        }
        public List<da_UserProfile> GetAll()
        {
            var data = db.st_UserProfile_GetAll().ToList();
            return data;
        }

        //
        public List<da_UserProfile> GetByEmail(string email)
        {
            return db.st_UserProfile_GetByEmail(email).ToList();
        }

        public List<da_UserProfile> GetByGroupId(int groupId)
        {
            return db.st_UserProfile_GetByGroupId(groupId).ToList();
        }

        public int CountByGroupId(int groupId)
        {
            return (int)db.st_UserProfile_CountByGroupId(groupId).Single();
        }

        public int Update(int userId, string userName, string phoneNumber, string email, string avatar, string bio, int groupId)
        {
            return (int)db.st_UserProfile_Update(userId, userName, phoneNumber, email, avatar, bio, groupId).Single();
        }

        public int UpdateGroup(int userId, int groupId)
        {
            return (int)db.st_UserProfile_UpdateGroup(userId, groupId).Single();
        }

        public List<da_UserProfile> Search(string userName, string phoneNumber, string email, int groupId)
        {
            return db.st_UserProfile_Search(userName, phoneNumber, email, groupId).ToList();
        }
    }
}
