﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMatrix.WebData;
using Egov.Service.Filters;
using Egov.Data.Models;
using Egov.Data.Business;


namespace Egov.Service.Controllers
{
    public class UserController : ApiController
    {   
        [HttpPost]
        public bool Login(string userName, string pass)
        {
            return true;
        }

        [HttpGet]
        public bool Registration(string userName, string pass)
        {
            //WebSecurity.InitializeDatabaseConnection("DefaultConnection", "da_UserProfile", "UserId", "UserName", autoCreateTables: true);
            WebSecurity.CreateUserAndAccount(userName, pass);
            return true;
        }

        [HttpGet]
        public da_UserProfile GetById(int userId)
        {
            bl_UserProfile bl_user = new bl_UserProfile();
            return bl_user.GetById(userId);
        }

        [HttpGet]
        public da_UserProfile GetByUserName(string userName)
        {
            bl_UserProfile bl_user = new bl_UserProfile();
            return bl_user.GetByUserName(userName);
        }

        [HttpGet]
        public List<da_UserProfile> GetAll()
        {
            bl_UserProfile bl_user = new bl_UserProfile();
            return bl_user.GetAll();
        }

        //

        bl_UserProfile bl_user = new bl_UserProfile();
        [HttpGet]
        public List<da_UserProfile> GetByEmail(string email)
        {
            return bl_user.GetByEmail(email);
        }

        [HttpGet]
        public List<da_UserProfile> GetByGroupId(int groupId)
        {
            return bl_user.GetByGroupId(groupId);
        }

        [HttpGet]
        public int CountByGroupId(int groupId)
        {
            return bl_user.CountByGroupId(groupId);
        }

        [HttpGet]
        public int Update(int userId, string userName, string phoneNumber, string email, string avatar, string bio, int groupId)
        {
            return bl_user.Update(userId, userName, phoneNumber, email, avatar, bio, groupId);
        }

        [HttpGet]
        public int UpdateGroup(int userId, int groupId)
        {
            return bl_user.UpdateGroup(userId, groupId);
        }

        [HttpGet]
        public List<da_UserProfile> Search(string userName, string phoneNumber, string email, int groupId)
        {
            return bl_user.Search(userName, phoneNumber, email, groupId);
        }
    }
}
