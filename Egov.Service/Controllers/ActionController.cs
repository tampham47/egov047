﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class ActionController : ApiController
    {
        bl_Action bl_action = new bl_Action();

        [HttpGet]
        public int Delete(Guid id)
        {
            return bl_action.Delete(id);
        }

        [HttpGet]
        public List<da_Action> GetAll()
        {
            return bl_action.GetAll();
        }

        [HttpGet]
        public da_Action GetById(Guid id)
        {
            return bl_action.GetById(id);
        }

        [HttpGet]
        public List<da_Action> GetByPlaceId(Guid placeId)
        {
            return bl_action.GetByPlaceId(placeId);
        }

        [HttpGet]
        public List<da_Action> GetByUserId(int userId)
        {
            return bl_action.GetByUserId(userId);
        }

        [HttpGet]
        public int Insert(Nullable<int> userId, Nullable<System.Guid> placeId, string actionType)
        {
            return bl_action.Insert(userId, placeId, actionType);
        }

        [HttpGet]
        public int Update(Nullable<System.Guid> id, Nullable<int> userId, Nullable<System.Guid> placeId, string actionType)
        {
            return bl_action.Update(id, userId, placeId, actionType);
        }

        [HttpGet]
        public List<da_Action> GetByType(string type)
        {
            return bl_action.GetByType(type);
        }
    }
}
