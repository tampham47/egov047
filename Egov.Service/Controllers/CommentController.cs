﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class CommentController : ApiController
    {
        bl_Comment bl_comment = new bl_Comment();

        [HttpGet]
        public List<da_Comment> GetAll()
        {
            return bl_comment.GetAll();
        }

        [HttpGet]
        public List<st_Comment_GetAllByStatus_Result> GetByStatus(bool status)
        {
            return bl_comment.GetAllByStatus(status);
        }

        [HttpGet]
        public List<da_Comment> GetByUserId(int userId)
        {
            return bl_comment.GetByUserId(userId);
        }

        [HttpGet]
        public List<da_Comment> GetByPlaceId(Guid placeId)
        {
            return bl_comment.GetByPlaceId(placeId);
        }

        [HttpGet]
        public List<st_CommentBy_Place_Status_Result> GetByPlaceStatus(Nullable<System.Guid> placeId, Nullable<bool> status)
        {
            return bl_comment.GetByPlaceStatus(placeId, status).ToList();
        }

        [HttpGet]
        public int Insert(Nullable<int> userId, string content, Nullable<System.Guid> placeId)
        {
            return bl_comment.Insert(userId, content, placeId, false);
        }

        [HttpGet]
        public int Update(Nullable<System.Guid> id, Nullable<int> userId, Nullable<System.Guid> placeId, string content, Nullable<bool> status)
        {
            return bl_comment.Update(id, userId, placeId, content, status);
        }

        [HttpGet]
        public int Delete(Guid id)
        {
            return bl_comment.Delete(id);
        }
    }
}
