﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class BannerController : ApiController
    {
        bl_Banner bl_banner = new bl_Banner();
        [HttpGet]
        public List<st_Banner_GetAll_Result> GetAll()
        {
            return bl_banner.GetAll();
        }

        [HttpGet]
        public List<st_Banner_GetAllByStatus_Result> GetByStatus(bool status)
        {
            return bl_banner.GetAllByStatus(status);
        }

        [HttpGet]
        public int Insert(Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            return bl_banner.Insert(userid, timeban, note, status);
        }

        [HttpGet]
        public int Update(Nullable<int> id, Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            return bl_banner.Update(id, userid, timeban, note,status);
        }
    }
}
