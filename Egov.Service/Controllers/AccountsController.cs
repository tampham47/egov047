﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Security.Cryptography;
using Egov.Data.Models;
using Egov.Data.Business;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Objects;

namespace Egov.Service.Controllers
{
    public class AccountsController : ApiController
    {
        Egov047Entities db = new Egov047Entities();
        [HttpGet]
        public int Login(string userName, string pass)
        {
            int idlogin = 0;
            string sqlConnectionString = @"Data Source=123.30.187.4;Initial Catalog=zxbsnudw_egov047;User ID=zxbsnudw_egov047;Password=09876poiuy";
            SqlConnection SQLconn = new SqlConnection(sqlConnectionString);
            SqlCommand cmd = new SqlCommand("st_Account_login", SQLconn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter outPutVal = new SqlParameter("@UserId", SqlDbType.Int);
            outPutVal.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(outPutVal);
            cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = userName;
            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = pass;
            SQLconn.Open();
            cmd.ExecuteNonQuery();
            SQLconn.Close();
            if (outPutVal.Value != DBNull.Value) idlogin = Convert.ToInt32(outPutVal.Value);
            return idlogin;
        }
        [HttpGet]
        public int getuser(string userName, string phone, string email, string avatar, string bio, int groupid)
        {
            int uId = -1;
            groupid = 1;
            var register = (from d in db.da_UserProfile where d.UserName.Equals(userName) select d).ToList();
            if (register.Count == 0 && userName != null)
            {

                string sqlConnectionString = @"Data Source=123.30.187.4;Initial Catalog=zxbsnudw_egov047;User ID=zxbsnudw_egov047;Password=09876poiuy";
                SqlConnection SQLconn = new SqlConnection(sqlConnectionString);
                SqlCommand cmd = new SqlCommand("st_UserProfile_AddUser_th3", SQLconn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter outPutVal = new SqlParameter("@UserId", SqlDbType.Int);
                outPutVal.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outPutVal);
                cmd.Parameters.Add("@userName", SqlDbType.NVarChar).Value = userName;
                cmd.Parameters.Add("@phoneNumer", SqlDbType.VarChar).Value = phone;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                cmd.Parameters.Add("@avatar", SqlDbType.VarChar).Value = avatar;
                cmd.Parameters.Add("@bio", SqlDbType.NVarChar).Value = bio;
                cmd.Parameters.Add("@groupId", SqlDbType.Int).Value = groupid;
                SQLconn.Open();
                cmd.ExecuteNonQuery();
                SQLconn.Close();
                if (outPutVal.Value != DBNull.Value) uId = Convert.ToInt32(outPutVal.Value);
                // return uId;
            }
            return uId;

        }
        [HttpGet]
        public Boolean Registration(string userName, string pass, string phone, string email, string avatar, string bio, int groupid)
        {
            int uid = getuser(userName, phone, email, avatar, bio, groupid);
            if (pass != null && uid != -1)
            {
                //int uid = getuser(userName, phone, email, avatar, bio, groupid);
                var regis = db.st_Account_getRegister(userName, pass, uid).ToList();
                if (regis.Count > 0) return true;
                return false;
            }
            return false;
        }
    }

}

