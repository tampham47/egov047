﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class BanController : ApiController
    {
        bl_Ban bl_ban = new bl_Ban();
        [HttpGet]
        public List<st_Ban_GetByAll_Result> GetAll()
        {
            return bl_ban.GetAll();
        }

        [HttpGet]
        public List<st_Ban_GetByStatus_Result> GetByStatus(bool status)
        {
            return bl_ban.GetAllByStatus(status);
        }

        [HttpGet]
        public int Insert(Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            return bl_ban.Insert(userid, timeban, note, status);
        }

        [HttpGet]
        public int Update(Nullable<int> id, Nullable<int> userid, Nullable<System.DateTime> timeban, string note, Nullable<bool> status)
        {
            return bl_ban.Update(id, userid, timeban, note,status);
        }

        [HttpGet]
        public int TimeBan(Nullable<int> id, Nullable<System.DateTime> timeban)
        {
            return bl_ban.TimeBan(id, timeban);
        }

        [HttpGet]
        public int Status(Nullable<int> id, Nullable<bool> status)
        {
            return bl_ban.Status(id, status);
        }
    }
}
