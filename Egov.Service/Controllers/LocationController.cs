﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class LocationController : ApiController
    {
        bl_Location bl_location = new bl_Location();

        [HttpGet]
        public int Delete(Guid id)
        {
            return bl_location.Delete(id);
        }

        [HttpGet]
        public List<da_Location> GetAll()
        {
            return bl_location.GetAll();
        }

        [HttpGet]
        public List<da_Location> GetByParentId(Guid parentId)
        {
            return bl_location.GetByParentId(parentId);
        }

        [HttpGet]
        public int Insert(string locationName, Nullable<System.Guid> parentId)
        {
            return bl_location.Insert(locationName, parentId);
        }

        [HttpGet]
        public int Update(Nullable<System.Guid> id, string locationName, Nullable<System.Guid> parentId)
        {
            return bl_location.Update(id, locationName, parentId);
        }
    }
}
