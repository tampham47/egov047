﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class GroupController : ApiController
    {
        bl_UserGroup bl_userGroup = new bl_UserGroup();

        [HttpGet]
        public int Insert(string groupName, string description)
        {
            return bl_userGroup.Insert(groupName, description);
        }

        [HttpGet]
        public List<da_UserGroup> GetAll()
        {
            return bl_userGroup.GetAll();
        }

        [HttpGet]
        public List<da_UserGroup> GetByID(int groupId)
        {
            return bl_userGroup.GetByID(groupId);
        }

        [HttpGet]
        public List<da_UserGroup> GetByName(string groupName)
        {
            return bl_userGroup.GetByName(groupName);
        }

        [HttpGet]
        public int Update(int groupId, string groupName, string description)
        {
            return bl_userGroup.Update(groupId, groupName, description);
        }

        [HttpGet]
        public int Delete(int groupId)
        {
            return bl_userGroup.Delete(groupId);
        }
    }
}
