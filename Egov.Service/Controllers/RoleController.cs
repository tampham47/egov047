﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;

namespace Egov.Service.Controllers
{
    public class RoleController : ApiController
    {
        bl_UserRole bl_userRole = new bl_UserRole();

        [HttpGet]
        public int Insert(int groupId, string roleName, string type)
        {
            return bl_userRole.Insert(groupId, roleName, type);
        }

        [HttpGet]
        public List<da_Roles> GetAll()
        {
            return bl_userRole.GetAll();
        }

        [HttpGet]
        public List<da_Roles> GetByGroupId(int groupId)
        {
            return bl_userRole.GetByGroupId(groupId);
        }

        [HttpGet]
        public List<da_Roles> GetByName(string roleName)
        {
            return bl_userRole.GetByName(roleName);
        }

        [HttpGet]
        public List<da_Roles> GetByIdAndType(int groupId, string type)
        {
            return bl_userRole.GetByIdAndType(groupId, type);
        }

        [HttpGet]
        public List<da_Roles> GetByType(string type)
        {
            return bl_userRole.GetByType(type);
        }

        [HttpGet]
        public int Delete(int groupId, string roleName)
        {
            return bl_userRole.Delete(groupId, roleName);
        }

        [HttpGet]
        public int DeleteByGroupId(int groupId)
        {
            return bl_userRole.DeleteByGroupId(groupId);
        }

        [HttpGet]
        public int DeleteByName(string roleName)
        {
            return bl_userRole.DeleteByName(roleName);
        }
    }
}
