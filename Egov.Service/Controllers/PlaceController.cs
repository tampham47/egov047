﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Egov.Data.Models;
using Egov.Data.Business;
using System.IO;

namespace Egov.Service.Controllers
{
    public class PlaceController : ApiController
    {
        private string serverPath = AppDomain.CurrentDomain.BaseDirectory + @"_ServiceData\Images\";
        private string GetTypImg(string base64Img)
        {
            string dataStr = base64Img.Substring(1, base64Img.IndexOf(';') - 1);
            string typeImg = dataStr.Substring(dataStr.LastIndexOf('/') + 1);
            return typeImg;
        }
        private string GetBase64(string base64Img)
        {
            return base64Img.Substring(base64Img.IndexOf(',') + 1);
        }
        private string SaveImageFromBase64(string base64Img)
        {
            string fileName = Guid.NewGuid() + "." + GetTypImg(base64Img);
            string filePath = serverPath + fileName;

            FileStream fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write, FileShare.None);
            byte[] datas = Convert.FromBase64String(GetBase64(base64Img));
            fs.Write(datas, 0, datas.Length);
            fs.Close();

            return fileName;
        }
        bl_Place bl_place = new bl_Place();

        [HttpGet]
        public List<da_Place> GetAll()
        {
            return bl_place.GetAll();
        }

        [HttpGet]
        public List<st_Place_GetAllByStatus_Result> GetByStatus(bool status)
        {
            return bl_place.GetAllByStatus(status);
        }

        [HttpGet]
        public List<st_PlaceBy_Location_Status_Result> GetByPlaceStatus(Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            return bl_place.GetByPlaceStatus(locationId, status).ToList();
        }

        [HttpGet]
        public List<da_Place> GetByLocationId(Guid locationId)
        {
            return bl_place.GetByLocationId(locationId);
        }

        [HttpGet]
        public List<da_Place> GetByUserId(int userId)
        {
            return bl_place.GetByUserId(userId);
        }

        [HttpGet]
        public Guid Insert(Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId)
        {
            if (priority == null) priority = 0;

            var id = bl_place.Insert2(userId, title, summary,
                description, latitude, longitude,
                priority, address, locationId, false);

            return id;
        }

        [HttpGet]
        public Guid InsertExpress(int userId, string title, string summary, string description)
        {
            var id = bl_place.Insert2(
                userId, title, summary, description, 
                1, 1, 1, 
                "Đại học công nghệ thông tin",
                null, true);

            bl_PlaceImage blPlaceImage = new bl_PlaceImage();
            Random rand = new Random();
            string fileName = "";

            fileName = rand.Next(99).ToString("D2") + ".jpg";
            blPlaceImage.Insert(id, fileName, "");

            fileName = rand.Next(99).ToString("D2") + ".jpg";
            blPlaceImage.Insert(id, fileName, "");

            return id;
        }

        public da_Place GetById(Guid id)
        {
            return bl_place.GetById(id);
        }

        [HttpGet]
        public Guid InsertWithImage(Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId, Nullable<bool> status, string img1, string img2, string img3)
        {
            var id = bl_place.Insert2(userId, title, summary,
                description, latitude, longitude,
                priority, address, locationId, status);

            bl_PlaceImage blPlaceImage = new bl_PlaceImage();
            string fileName = "";
            if (img1 != null)
            {
                fileName = SaveImageFromBase64(img1);
                blPlaceImage.Insert(id, fileName, "");
            }
            if (img2 != null)
            {
                fileName = SaveImageFromBase64(img2);
                blPlaceImage.Insert(id, fileName, "");
            }
            if (img3 != null)
            {
                fileName = SaveImageFromBase64(img3);
                blPlaceImage.Insert(id, fileName, "");
            }

            return id;
        }

        [HttpGet]
        public string InsertImage(Guid placeId, string img)
        {
            bl_PlaceImage blPlaceImage = new bl_PlaceImage();

            string fileName = SaveImageFromBase64(img);
            blPlaceImage.Insert(placeId, fileName, "");

            return fileName;
        }

        [HttpGet]
        public string AddImage(Guid placeId, string img)
        {
            bl_PlaceImage blPlaceImage = new bl_PlaceImage();

            blPlaceImage.Insert(placeId, img, "");

            return img;
        }

        [HttpPost]
        public string PTest(string img)
        {
            var id = Guid.Parse("e1bc70c8-f376-4d07-a014-52d01f2340d0");
            bl_PlaceImage blPlaceImage = new bl_PlaceImage();

            string fileName = SaveImageFromBase64(img);
            blPlaceImage.Insert(id, fileName, "");

            return fileName;
        }

        [HttpGet]
        public int Update(Nullable<System.Guid> id, Nullable<int> userId, string title, string summary, string description, Nullable<double> latitude, Nullable<double> longitude, Nullable<int> priority, string address, Nullable<System.Guid> locationId, Nullable<bool> status)
        {
            return bl_place.Update(id, userId, title, summary, 
                description, latitude, longitude,
                priority, address, locationId, status);
        }

        [HttpGet]
        public int Delete(Guid id)
        {
            return bl_place.Delete(id);
        }
    }
}
